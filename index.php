<?php

require __DIR__ . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors',1);
ini_set('memory_limit', '-1');
set_time_limit(0);

//use PHPExcel_IOFactory;
$file = __DIR__ . '/xls/kurdish.xlsx';

$objReader = PHPExcel_IOFactory::createReader('Excel2007');

$objPHPExcel = $objReader->load($file);

/** @var PHPExcel_Writer_CSV $objWriter */
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
$objWriter->setDelimiter('|');
$objWriter->setEnclosure('');

echo $csvFile = __DIR__ . '/csv/kurdish.csv';

touch($csvFile);

$objWriter->save($csvFile);
return "success";